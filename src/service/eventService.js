import jwtDecode from 'jwt-decode';

async function getAllEvents() {
  try {
    const token = localStorage.getItem('x-access-token');
    const description = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token,
      },
      credentials: 'include',
    };
    const userId = await getUsedId(token);
    return fetch(
      `http://localhost:3127/api/events/getByUserId/${userId}`,
      description
    )
      .then((res) => res.json())
      .then((data) => data)
      .catch((err) => {
        console.log(err);
      });
  } catch (error) {
    alert(error);
  }
}

async function addNewEvent(newEvent) {
  console.log(newEvent);
  try {
    const token = localStorage.getItem('x-access-token');
    const userId = await getUsedId(token);
    const data = {
      event: newEvent.event,
      startDate: newEvent.startDate,
      endDate: newEvent.endDate,
      recurrence: newEvent.recurrence,
      eventDuration: newEvent.eventDuration,
      userId: userId,
    };
    const description = {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token,
      },
      body: JSON.stringify(data),
    };
    console.log(localStorage.getItem('x-access-token'));
    await fetch(`http://localhost:3127/api/events/add`, description);
  } catch (error) {
    alert(error);
  }
}

async function deleteEvent(eventId) {
  return fetch(`http://localhost:8080/deleteEvent/${eventId}`, {
    method: 'DELETE',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'x-access-token': localStorage.getItem('x-access-token'),
    },
  });
}

async function getUsedId(token) {
  return jwtDecode(token).id;
}

export { getAllEvents, addNewEvent, deleteEvent };
