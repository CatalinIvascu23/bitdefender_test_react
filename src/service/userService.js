async function registerUser(userCredentials) {
  const response = await fetch('http://localhost:3127/api/users/register', {
    method: 'POST',
    credentials: 'include',
    crossDomain: true,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(userCredentials),
  });
  return response;
}

async function loginUser(userCredentials) {
  const response = await fetch('http://localhost:3127/api/users/login', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(userCredentials),
  });
  localStorage.setItem(
    'x-access-token',
    response.headers.get('x-access-token')
  );
  return response;
}

export { registerUser, loginUser };
