import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { loginUser } from '../service/userService';
import './authentication.css';

function Login() {
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const history = useHistory();

    const login = async (event) => {
        event.preventDefault();
        const response = await loginUser({
            userName,
            password
        });
        if (response.status === 200) {
            history.push("/");
        }
        else {
            alert('Something went wrong, could not log you in');
        }
    }

    const handleUserNameChange = (event) => {
        setUserName(event.target.value);
    }

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    }

    return (
        <div className="authContainer">
            <p>Login</p>
            <form
                className="authForm"
                onSubmit={(event) => login(event)}>
                <input
                    className="authInput"
                    onChange={handleUserNameChange}
                    value={userName}
                    placeholder="username"
                />
                <input
                    className="authInput"
                    onChange={handlePasswordChange}
                    value={password}
                    placeholder="password"
                    type="password"
                />
                <button
                    className="authButton"
                    id="registerButton"
                    type="submit"
                >Login</button>
            </form>
        </div>
    );
}

export default Login;