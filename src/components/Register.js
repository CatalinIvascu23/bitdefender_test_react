import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { registerUser } from '../service/userService';
import './authentication.css';

function Register() {
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const history = useHistory();

    const register = async (event) => {
        event.preventDefault();
        const registerResponse = await registerUser({
            userName, 
            password, 
            confirmPassword
        });
        if (registerResponse.status === 201) {
            history.push('/login');
        } else {
            alert('Could not register user');
        }
    }

    const handleUserNameChange = (event) => {
        setUserName(event.target.value);
    }

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    }

    const handleConfirmPasswordChange = (event) => {
        setConfirmPassword(event.target.value);
    }

    return (
        <div className="authContainer">
            <p>Register</p>
            <form
                className="authForm"
                onSubmit={(event) => register(event)}>
                <input
                    className="authInput"
                    onChange={handleUserNameChange}
                    value={userName}
                    placeholder="username"
                />
                <input
                    className="authInput"
                    onChange={handlePasswordChange}
                    value={password}
                    placeholder="password"
                    type="password"
                />
                 <input
                    className="authInput"
                    onChange={handleConfirmPasswordChange}
                    value={confirmPassword}
                    placeholder="confirm password"
                    type="password"
                />
                <button
                    className="authButton"
                    id="registerButton"
                    type="submit"
                >Register</button>
            </form>
        </div>
    );
}

export default Register;