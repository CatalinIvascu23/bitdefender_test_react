const datesIntervalInDays = (startDate, endDate) => {
	let daysInMilliseconds = new Date(endDate).getTime() - new Date(startDate).getTime();
	return parseInt(daysInMilliseconds / (60 * 60 * 24 * 1000));
}

export {
	datesIntervalInDays
}