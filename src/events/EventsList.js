import React, { useEffect, useState } from 'react';
import Event from './Event';
import AddNewEvent from './AddNewEvent';
import { addNewEvent, getAllEvents } from '../service/eventService';
import { datesIntervalInDays } from '../helper';

function EventsList() {
  const [eventsList, setEventsList] = useState([]);

  useEffect(() => {
    getAllEvents().then((events) => {
      setEventsList(events);
    });
  }, [JSON.stringify(eventsList)]);

  const displayEventsList = () => {
    return eventsList.map((eventItem) => {
      return <Event key={eventItem._id} eventItem={eventItem} />;
    });
  };

  const createNewEvent = async (newEvent) => {
    let eventDuration = datesIntervalInDays(
      newEvent.startDate,
      newEvent.endDate
    );
    await addNewEvent({ ...newEvent, eventDuration });
  };

  return (
    <div id="EventsList">
      <AddNewEvent
        eventName={''}
        startDate={null}
        endDate={null}
        recurrenceText=""
        buttonDescription="Add Event"
        handleButtonClick={createNewEvent}
      />
      {displayEventsList()}
    </div>
  );
}

export default EventsList;
