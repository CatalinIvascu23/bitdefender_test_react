import React, { useState } from 'react';
import { deleteEvent } from '../service/eventService';
import AddNewEvent from './AddNewEvent';
import './event.css';
import { datesIntervalInDays } from '../helper';

function Event({ eventItem }) {
  const [editButtonIsClicked, setEditButtonIsClicked] = useState(false);

  const handleDeleteEvent = async () => {
    const response = await deleteEvent(eventItem._id);
    if (response.status === 200) {
      window.location.reload();
    } else {
      alert('Could not delete event');
    }
  };

  const displayDependingOnDays = () => {
    if (
      new Date(eventItem.startDate).getTime() ===
      new Date(eventItem.endDate).getTime()
    ) {
      return <div>Date: {eventItem.startDate}</div>;
    } else {
      return (
        <>
          <div>Start Date: {eventItem.startDate}</div>
          <div>End Date: {eventItem.endDate}</div>
          <div>
            Duration:{' '}
            {datesIntervalInDays(eventItem.startDate, eventItem.endDate)} days
          </div>
        </>
      );
    }
  };

  const handleEditEvent = () => setEditButtonIsClicked(true);

  const cancelButton = () => {
    return (
      <button onClick={() => setEditButtonIsClicked(false)}>Cancel</button>
    );
  };

  const displayItem = () => {
    return (
      <>
        <div>Name: {eventItem.event}</div>
        {displayDependingOnDays()}
        {eventItem.recurrence !== '' ? (
          <div> Recurrence: {eventItem.recurrence} </div>
        ) : (
          <></>
        )}
        <button onClick={handleDeleteEvent}>Delete</button>
        <button onClick={handleEditEvent}>Edit</button>
      </>
    );
  };

  const displayItemOnEdit = () => {
    return (
      <AddNewEvent
        id={eventItem.id}
        name={eventItem.event}
        startDate={eventItem.startDate}
        endDate={eventItem.endDate}
        recurrenceText={eventItem.reccurence}
        buttonDescription="Submit"
        cancelButton={cancelButton()}
      />
    );
  };

  return (
    <div className="event">
      {!editButtonIsClicked ? displayItem() : displayItemOnEdit()}
    </div>
  );
}

export default Event;
