import React, { useState } from 'react'
import TextField from '@material-ui/core/TextField';
import DateRangePicker from '@material-ui/lab/DateRangePicker';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import Box from '@material-ui/core/Box';

function AddNewEvent({ id, name, startDate, endDate, recurrenceText, buttonDescription, handleButtonClick, cancelButton }) {
	const [newEvent, setNewEvent] = useState(name)
	const [dates, setDates] = useState([startDate, endDate]);
	const [recurrence, setRecurrence] = useState(recurrenceText);

	return (
		<div className='addEventSection'>

			<label htmlFor='eventNameInput'>Name:
				<input id='eventNameInput'
					value={newEvent}
					onChange={event => setNewEvent(event.target.value)} />
			</label>

			<LocalizationProvider dateAdapter={AdapterDateFns}>
				<DateRangePicker
					startText='Start Date'
					endText='End Date'
					value={dates}
					onChange={newValue => { setDates(newValue) }}
					renderInput={(startProps, endProps) => (
						<>
							<TextField {...startProps} />
							<Box sx={{ mx: 1 }}> to </Box>
							<TextField {...endProps} />
						</>
					)}
				/>
			</LocalizationProvider>

			<label htmlFor='eventRecurrenceInput'>Recurrence:
				<input id='eventrecurrenceInput'
					value={recurrence}
					onChange={event => setRecurrence(event.target.value)} />
			</label>

			<button onClick={async () => {
				let eventObj = {
					event: newEvent,
					startDate: dates[0],
					endDate: dates[1],
					recurrence: recurrence
				}
				await handleButtonClick(eventObj)
			}}>
				{buttonDescription}</button>
			{cancelButton}
		</div>
	)
}

export default AddNewEvent
